package checkers3d.graphics;

import java.util.HashSet;
import java.util.Set;

import com.jme3.app.SimpleApplication;
import com.jme3.asset.plugins.FileLocator;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Ray;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Box;

import checkers3d.logic.Chessboard;
import checkers3d.logic.IChessboard;

public class MyApplication extends SimpleApplication {

    private IChessboard gameLogic;

    private Node boardNode, corner, player, playerPivot;
    private ColorRGBA perspective = ColorRGBA.Black;
    private Box surfBox;

    private Set<Piece> pieces;
    private boolean selected;
    private Vector2f selectedCoordinate;
    private Piece selectedPiece;

    // METODO CHIAMATO AL LANCIO
    @Override
    public void simpleInitApp() {

        // Risorse
        assetManager.registerLocator("assets", FileLocator.class);

        // Inizializzazioni
        gameLogic = new Chessboard();
        pieces = new HashSet<Piece>(Chessboard.FULL_ROWS * Chessboard.SIZE / 2);
        boardNode = new Node("board");
        corner = new Node("Origine coordinate scacchiera");
        player = new Node("Player: node for the camera to follow");
        playerPivot = new Node("Player's pivot for rotation around chessboard");

        // Scene Graph
        rootNode.attachChild(boardNode);
        boardNode.attachChild(corner);
        boardNode.attachChild(playerPivot);
        playerPivot.attachChild(player);

        // Sfondo
        viewPort.setBackgroundColor(new ColorRGBA(0.7f, 0.8f, 1f, 1f));

        // Camera
        flyCam.setEnabled(false);

        // Giocatore
        positionPlayer();

        // Luce
        // Ambientale
        AmbientLight sun = new AmbientLight();
        sun.setColor(ColorRGBA.LightGray);
        rootNode.addLight(sun);
        // Direzionale
        DirectionalLight lamp = new DirectionalLight();
        lamp.setDirection(new Vector3f(-1, 0.5f, -0.2f));
        lamp.setColor(ColorRGBA.White);
        rootNode.addLight(lamp);

        // Scacchiera
        // Base
        Box baseBox = new Box(4, 0.25f, 4); // Mesh: poliedro
        Geometry base = new Geometry("Board base", baseBox);
        Material wood = new Material(assetManager,
                "Common/MatDefs/Misc/Unshaded.j3md");
        wood.setColor("Color", ColorRGBA.DarkGray);
        base.setMaterial(wood);
        boardNode.attachChild(base);
        // Superficie
        surfBox = new Box(4, 0.01f, 4);
        Geometry surface = new Geometry("Board surface", surfBox);
        surface.setLocalTranslation(0f, 0.25f, 0f);
        Material surfMat = new Material(assetManager,
                "Common/MatDefs/Misc/Unshaded.j3md");
        surfMat.setTexture("ColorMap",
                assetManager.loadTexture("Textures/checker.jpg"));
        surface.setMaterial(surfMat);
        boardNode.attachChild(surface);

        // Riferimento coordinate 2D schacchiera
        corner.setLocalTranslation(-3.5f,
                surface.getLocalTranslation().y + 0.01f + Piece.HEIGHT, -3.5f);

        // Scritta iniziale
        guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
        BitmapText text = new BitmapText(guiFont, false);
        text.setSize(guiFont.getCharSet().getRenderedSize());
        text.setText("PRESS ENTER TO START A NEW GAME");
        text.setLocalTranslation(400 - text.getLineWidth() / 2,
                300 + text.getLineHeight() / 2, 0);
        guiNode.attachChild(text);

        // Tasti
        initKeys();
    }

    // METODO CHIAMATO AD OGNI FRAME (tpf = time per frame)
    @Override
    public void simpleUpdate(float tpf) {

        for (Piece p : pieces) {
            // Cancella pezzi catturati
            if (gameLogic.isEmpty(p.get2DCoordinate())) {
                corner.detachChild(p);
                // ANIMAZIONE: muove il pezzo verso la sua destinazione
            } else if (p.getTargetLocation()
                    .distance(p.getLocalTranslation()) > tpf)
                p.move(p.getTargetLocation().subtract(p.getLocalTranslation())
                        .normalize().mult(2 * tpf));
            else
                p.setLocalTranslation(p.getTargetLocation());
        }
    }

    private void startGame() {

        // Inizia/resetta (logica)
        gameLogic.begin();
        // Posiziona i pezzi (grafica)
        pieces.clear();
        for (int i = 0; i < Chessboard.SIZE; i++) {
            for (int j = 0; j < Chessboard.SIZE; j++) {
                Vector2f location2D = new Vector2f(i, j);

                if (!gameLogic.isEmpty(location2D)) {
                    Vector3f location3D = new Vector3f(location2D.y, 0,
                            location2D.x);
                    Piece p = new Piece(gameLogic.pieceAt(location2D),
                            location3D, this);
                    pieces.add(p);
                    p.setLocalTranslation(location3D);
                    corner.attachChild(p);
                }
            }
        }

        // Imposta prospettiva
        perspective = gameLogic.getCurrent();

        // Attiva risposta click
        initMouse();
    }

    // Mette il giocatore in una posizione di default
    private void positionPlayer() {
        if (perspective.equals(ColorRGBA.Black))
            player.setLocalTranslation(0, 8, 11);
        else
            player.setLocalTranslation(0, 8, -11);
        adjustCamera();
    }

    // Fissa la telecamera sul giocatore, rivolta verso il centro della
    // scacchiera
    private void adjustCamera() {
        cam.setLocation(player.getWorldTranslation());
        cam.lookAt(new Vector3f(0, 0, 0), new Vector3f(0, 1, 0));
    }

    // Imposta comandi tastiera
    private void initKeys() {

        // tasto->comando
        inputManager.addMapping("Start", new KeyTrigger(KeyInput.KEY_RETURN));
        inputManager.addMapping("RotateC", new KeyTrigger(KeyInput.KEY_LEFT));
        inputManager.addMapping("RotateCC", new KeyTrigger(KeyInput.KEY_RIGHT));
        inputManager.addMapping("ZoomOut", new KeyTrigger(KeyInput.KEY_DOWN));
        inputManager.addMapping("ZoomIn", new KeyTrigger(KeyInput.KEY_UP));
        inputManager.addMapping("RestoreAngle",
                new KeyTrigger(KeyInput.KEY_SPACE));

        // comando->evento
        inputManager.addListener(playerListener, "RotateC", "RotateCC",
                "ZoomOut", "ZoomIn");
        inputManager.addListener(actionKeyListener, "Start", "RestoreAngle");

    }

    // Listener per movimenti giocatore
    private AnalogListener playerListener = new AnalogListener() {

        @Override
        public void onAnalog(String name, float value, float tpf) {
            if (name.equals("RotateC")) {
                playerPivot.rotate(0, value * speed, 0);
            }
            if (name.equals("RotateCC")) {
                playerPivot.rotate(0, -value * speed, 0);
            }
            if (name.equals("ZoomOut")) {
                if (player.getLocalTranslation().length() < 30)
                    player.move(
                            player.getLocalTranslation().mult(value * speed));
            }
            if (name.equals("ZoomIn")) {
                if (player.getLocalTranslation().length() > 6)
                    player.move(
                            player.getLocalTranslation().mult(-value * speed));
            }

            adjustCamera();
        }

    };

    // Listener per comandi di reset
    private ActionListener actionKeyListener = new ActionListener() {

        @Override
        public void onAction(String name, boolean isPressed, float tpf) {
            if (name.equals("RestoreAngle") && !isPressed) {
                // Resetta visuale
                playerPivot
                        .setLocalRotation(new Quaternion().fromAngles(0, 0, 0));
                positionPlayer();
            }
            if (name.equals("Start") && !isPressed) {
                // Resetta gioco
                guiNode.detachAllChildren();
                corner.detachAllChildren();
                startGame();
            }
        }

    };

    // Imposta comandi mouse
    private void initMouse() {
        // pulsante->comando
        inputManager.addMapping("Select",
                new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
        // comando->evento
        inputManager.addListener(actionMouseListener, "Select");
    }

    // Listener per click mouse
    private ActionListener actionMouseListener = new ActionListener() {

        @Override
        public void onAction(String name, boolean isPressed, float tpf) {

            if (name.equals("Select") && !isPressed) {

                // RAY CASTING
                CollisionResults results = new CollisionResults();
                Vector2f click2d = inputManager.getCursorPosition();
                Vector3f click3d = cam.getWorldCoordinates(
                        new Vector2f(click2d.x, click2d.y), 0f).clone();
                Vector3f dir = cam
                        .getWorldCoordinates(new Vector2f(click2d.x, click2d.y),
                                1f)
                        .subtractLocal(click3d).normalizeLocal();
                Ray ray = new Ray(click3d, dir);
                boardNode.collideWith(ray, results);

                if (results.size() > 0)
                    select(results.getClosestCollision());
            }

        }

        // Cosa fare con l'oggetto selezionato
        private void select(CollisionResult closest) {

            // Riappoggia eventuale pezzo sollevato
            if (selected)
                selectedPiece.setTargetLocation(
                        selectedPiece.getTargetLocation().setY(0.01f));

            // Solleva pezzo da muovere (se il pezzo è disponibile)
            if (closest.getGeometry() instanceof Piece) {
                Vector3f rawSrc = closest.getGeometry().getLocalTranslation();
                selectedCoordinate = new Vector2f((float) Math.floor(rawSrc.z),
                        (float) Math.floor(rawSrc.x));
                if (gameLogic.isMovable(selectedCoordinate)) {
                    selected = true;
                    selectedPiece = (Piece) closest.getGeometry();
                    selectedPiece.setTargetLocation(
                            selectedPiece.getTargetLocation().setY(0.51f));
                }

                // Imposta destinazione pezzo (se la mossa è valida)
            } else if (selected) {
                selected = false;
                Vector3f rawDest = closest.getContactPoint().subtract(
                        corner.getLocalTranslation().add(0, -Piece.HEIGHT, 0));
                Vector3f dest3D = new Vector3f(
                        (float) Math.floor(rawDest.x + 0.5), rawDest.y,
                        (float) Math.floor(rawDest.z + 0.5));
                Vector2f dest2D = new Vector2f(dest3D.z, dest3D.x);

                if (gameLogic.move(selectedCoordinate, dest2D)) {
                    selectedPiece.setTargetLocation(dest3D);
                    perspective = gameLogic.getCurrent();
                }
            }
        }

    };
}
