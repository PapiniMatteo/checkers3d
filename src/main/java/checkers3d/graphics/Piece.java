package checkers3d.graphics;

import com.jme3.app.SimpleApplication;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Cylinder;

public class Piece extends Geometry {

    private Vector3f targetLocation;
    public static final float HEIGHT = 0.1f;

    public Piece(ColorRGBA color, Vector3f intialLocation,
            SimpleApplication app) {
        // Geometria
        super("Piece", new Cylinder(30, 30, 0.4f, HEIGHT, true));

        // Materiale
        Material mat = new Material(app.getAssetManager(),
                "Common/MatDefs/Light/Lighting.j3md");
        mat.setBoolean("UseMaterialColors", true);
        mat.setColor("Ambient", color); // Colore
        mat.setColor("Diffuse", ColorRGBA.White); // Riflesso
        this.setMaterial(mat);

        // Posizione
        this.setLocalRotation(
                new Quaternion().fromAngles((float) Math.toRadians(90), 0, 0));
        this.targetLocation = intialLocation;
    }

    public Vector3f getTargetLocation() {
        return new Vector3f(targetLocation);
    }

    public void setTargetLocation(Vector3f targetLocation) {
        this.targetLocation = targetLocation;
    }

    public Vector2f get2DCoordinate() {
        return new Vector2f(targetLocation.z, targetLocation.x);
    }

}
