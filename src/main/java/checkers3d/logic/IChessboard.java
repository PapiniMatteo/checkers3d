package checkers3d.logic;

import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;

public interface IChessboard {

    public void begin();

    public boolean isMovable(Vector2f src);

    public boolean move(Vector2f src, Vector2f dest);

    public ColorRGBA pieceAt(Vector2f coordinate);

    public boolean isEmpty(Vector2f coordinate);

    public ColorRGBA getCurrent();

}
