package checkers3d.logic;

import java.util.HashMap;
import java.util.Map;

import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;

public class Chessboard implements IChessboard {

    public static final int SIZE = 8;
    public static final int FULL_ROWS = 3;
    public static final String ERR = "Illegal move";

    private Map<Vector2f, ColorRGBA> board;
    private ColorRGBA current;

    public Chessboard() {
        board = new HashMap<Vector2f, ColorRGBA>(SIZE * SIZE);
    }

    @Override
    public void begin() {
        board.clear();

        for (int i = 0; i < FULL_ROWS; i++) {
            int first = i % 2 == 0 ? 1 : 0;
            for (int j = first; j < SIZE; j += 2) {
                board.put(new Vector2f(i, j), ColorRGBA.White);
                board.put(new Vector2f(SIZE - i - 1, SIZE - j - 1),
                        ColorRGBA.Black);
            }
        }

        current = ColorRGBA.Black;
    }

    @Override
    public boolean isMovable(Vector2f src) {
        if (!board.containsKey(src) || !board.get(src).equals(current))
            return false;
        return true;
    }

    @Override
    public boolean move(Vector2f src, Vector2f dest) {

        if (!isMovable(src))
            return false;

        if (board.containsKey(dest))
            return false;

        if (dest.x < 0 || dest.x >= SIZE || dest.y < 0 || dest.y >= SIZE)
            return false;

        int deX = (int) (dest.x - src.x);
        int deY = (int) (dest.y - src.y);

        if (Math.abs(deX) != Math.abs(deY) || Math.abs(deX) > 2)
            return false;

        if ((current.equals(ColorRGBA.Black)) && deX > 0
                || (current.equals(ColorRGBA.White) && deX < 0))
            return false;

        if (Math.abs(deX) == 2) {
            Vector2f traversed = new Vector2f((src.x + dest.x) / 2,
                    (src.y + dest.y) / 2);
            if (!board.containsKey(traversed)
                    || board.get(traversed).equals(current))
                return false;
            board.remove(traversed);
        }

        board.put(dest, board.remove(src));
        current = opposite(current);
        return true;
    }

    @Override
    public ColorRGBA pieceAt(Vector2f coordinate) {
        return board.get(coordinate);
    }

    @Override
    public boolean isEmpty(Vector2f coordinate) {
        return !board.containsKey(coordinate);
    }

    @Override
    public ColorRGBA getCurrent() {
        return current;
    }

    private static ColorRGBA opposite(ColorRGBA c) {
        return c == ColorRGBA.Black ? ColorRGBA.White : ColorRGBA.Black;
    }
}
