package checkers3d;

import com.jme3.system.AppSettings;

import checkers3d.graphics.MyApplication;

public class Main {

    public static void main(String[] args) {

        MyApplication app = new MyApplication();

        // Default settings, no popup
        app.setShowSettings(false);
        AppSettings settings = new AppSettings(true);
        settings.put("Width", 800);
        settings.put("Height", 600);
        settings.put("Title", "Checkers 3D");
        settings.put("VSync", true);
        settings.put("Samples", 4);
        app.setSettings(settings);
        ////

        app.start();
    }

}
